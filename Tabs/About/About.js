import React, { PureComponent } from "react";
import { View, Image, Linking } from "react-native";
import { withNavigation } from "react-navigation";
import Swiper from "react-native-swiper";
import { Button, Icon, Text } from "react-native-elements";

import VideoButton from "@Components/VideoButton";
import HTMLRenderer from "@Components/HTMLRenderer/html";

import Layout from "@Constants/Layout";
import withRefScroll from "@HOCS/withRefScroll";

import styles from "./styles";

const About = ({ details, entitle, navigation }) => (
  <View style={styles.container}>
    <View style={styles.card}>
      <Image style={styles.image} source={{ uri: details.mainImage }} />
      {Boolean(details.hasVideo) && (
        <View style={styles.cardText}>
          <VideoButton id={details.video} />
        </View>
      )}
    </View>
    <View style={styles.textContainer}>
      <Text style={styles.heading}>{details.subtitle}</Text>
      <HTMLRenderer body={details.text} noOffset />
    </View>
    <View style={styles.bookingContainer}>
      <Button
        onPress={() => navigation.navigate("somePage")}
        buttonStyle={styles.bookingButtonStyle}
        titleStyle={styles.bookingTitleStyle}
        icon={
          <Icon
            type="material-community"
            name="square-edit-outline"
            size={Layout.isSmallDevice ? 16 : 20}
            color="white"
          />
        }
        title="SOME PAGE"
      />
    </View>
  </View>
);

export default withRefScroll(withNavigation(About));
