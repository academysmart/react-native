import React, { PureComponent } from "react";
import { View, FlatList, TouchableOpacity, Platform } from "react-native";
import { Icon } from "react-native-elements";
import FastImage from "react-native-fast-image";
import Lightbox from "react-native-lightbox";

import Colors from "@Constants/Colors";

import styles from "./styles";

class Gallery extends PureComponent {
  renderItem = ({ item, index }) => {
    const isEven = !Boolean((index + 1) % 2); // add additional styles for even items

    return (
      <View style={[styles.item, isEven && styles.evenItem]}>
        <Lightbox
          navigator={this.props.navigation}
          renderHeader={(close) =>
            Platform.OS === "ios" ? this.renderLightBoxHeader(close) : null
          }
          activeProps={{
            resizeMode: FastImage.resizeMode.contain,
          }}>
          <FastImage
            style={styles.image}
            source={{
              uri: item,
              priority: FastImage.priority.high,
              cache: FastImage.cacheControl.immutable,
            }}
            resizeMode={FastImage.resizeMode.cover}
          />
        </Lightbox>
      </View>
    );
  };

  renderLightBoxHeader = (close) => (
    <TouchableOpacity style={styles.lightboxHeader} onPress={close}>
      <Icon type="antdesign" name="close" size={32} color={Colors.white} />
    </TouchableOpacity>
  );

  render() {
    return (
      <FlatList
        style={styles.container}
        data={this.props.details.images}
        renderItem={this.renderItem}
        keyExtractor={(_, index) => String(index)}
        numColumns={2}
      />
    );
  }
}

export default Gallery;
