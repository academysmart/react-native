import React, { Component } from "react";
import { ScrollView, StyleSheet } from "react-native";

const styles = StyleSheet.create({ fill: { flexGrow: 1 } });

const withRefScroll = Screen => {
  class WithScroll extends Component {
    scrollToTop = () => {
      this._scroll.scrollTo({ y: 0 });
    };

    render() {
      return (
        <ScrollView
          ref={scroll => (this._scroll = scroll)}
          bounces={false}
          contentContainerStyle={styles.fill}
        >
          <Screen {...this.props} />
        </ScrollView>
      );
    }
  }

  return WithScroll;
};

export default withRefScroll;
