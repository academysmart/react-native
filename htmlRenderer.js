import React from "react";
import { View, Linking, StyleSheet } from "react-native";
import HTML from "react-native-render-html";

import ScaledImage from "@Components/ScaledImage/ScaledImage";
import VideoButton from "@Components/VideoButton";

import htmlStyles from "./htmlStyles";

const navLink = (_, link) => Linking.openURL(link);

const HTMLRenderer = ({ body, noOffset }) => (
  <HTML
    html={body}
    containerStyle={[styles.htmlContainer, noOffset && styles.noOffset]}
    tagsStyles={htmlStyles}
    onLinkPress={navLink}
    renderers={{
      img: ({ src }) => <ScaledImage uri={src} />,
      iframe: ({ src }) => {
        const id = src.slice(src.lastIndexOf("/") + 1);

        return (
          <View style={styles.videoContainer}>
            <VideoButton hasPreview id={id} />
          </View>
        );
      },
    }}
  />
);

const styles = StyleSheet.create({
  videoContainer: { width: "100%", height: 300 },
  htmlContainer: {
    marginTop: 20,
    marginBottom: 50,
    paddingHorizontal: 20,
  },
  noOffset: {
    paddingHorizontal: 0,
  },
});

export default HTMLRenderer;
