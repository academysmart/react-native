import React, { Component } from "react";
import { connect } from "react-redux";
import { View, Platform, Share } from "react-native";
import { Icon, ButtonGroup, Button } from "react-native-elements";
import Swiper from "react-native-swiper";

import About from "./Tabs/About/About";
import Rates from "./Tabs/Rates/Rates";
import Gallery from "./Tabs/Gallery/Gallery";
import Faq from "./Tabs/FAQ/FAQ";

import ProgressBar from "@Components/ProgressBar/ProgressBar";
import Header from "@Components/Header/Header";
import { getCamp } from "@Modules/camps/actions";
import { startSpinner } from "@Modules/app/actions";

import Colors from "@Constants/Colors";
import Layout from "@Constants/Layout";
import CAMPS from "@Constants/CampsConfig";
import uid from "@Utils/uid";

import styles from "./Tabs/styles";

const BUTTONS = ["ABOUT", "RATES", "GALLERY", "FAQ"];

class CampsNavigator extends Component {
  state = {
    selectedIndex: 0,
    campID: null,
    header: "",
    entitle: "",
    resetFAQ: false,
  };

  static defaultProps = {
    airstrip: {
      latitude: -18.801112,
      longitude: 46.598417,
    },
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const {
      navigation: { state },
    } = nextProps;

    const privateSuites = !/camp/i.test(state.params.header);

    return {
      ...prevState,
      campID: state.params.id,
      header: privateSuites ? "Private Suites" : state.params.header,
      entitle: state.params.header,
    };
  }

  scrollToTop = () => {
    this.aboutScroll.scrollToTop();
    this.ratesScroll.scrollToTop();
    this.faqScroll.scrollToTop();
  };

  updateIndex = (selectedIndex) => {
    if (selectedIndex === this.state.selectedIndex) return;

    this.swiper.scrollBy(selectedIndex - this.state.selectedIndex);
    this.setState({ selectedIndex, resetFAQ: true }, this.scrollToTop);
  };

  shareSocial = () => {
    const { airstrip } = this.props;
    const timeout = Platform.OS === "ios" ? 100 : 300;

    try {
      setTimeout(
        () =>
          Share.share(
            {
              message: `http://www.google.com/maps/place/${airstrip.latitude},${
                airstrip.longitude
              }`,
              title: "Airstrip Location",
            },
            {
              // iOS only:
              excludedActivityTypes: [
                "com.apple.UIKit.activity.TypeAirDrop",
                "com.apple.UIKit.activity.TypeAddToReadingList",
                "com.apple.UIKit.activity.TypeAssignToContact",
                "com.apple.UIKit.activity.TypePostToFlickr",
                "com.apple.UIKit.activity.TypeOpenInIBooks",
                "com.apple.UIKit.activity.TypePrint",
                "com.apple.UIKit.activity.TypeSaveToCameraRoll",
                "com.apple.UIKit.activity.TypeMarkupAsPDF",
              ],
            }
          ),
        timeout
      );
    } catch (error) {
      alert(error.message);
    }
  };

  goToCampMap = () => {
    const { header } = Object.values(CAMPS).find((camp) => camp.id === this.state.campID);

    this.props.navigation.navigate("CampMap", { header });
  };

  componentDidMount() {
    const { startSpinner, getCamp, navigation, campDetails } = this.props;
    if (!Boolean(campDetails)) {
      startSpinner();
    }
    getCamp({
      id: this.state.campID,
      navigator: navigation,
    });
  }

  render() {
    const { campDetails, navigation, showSpinner } = this.props;

    const { selectedIndex, entitle, subtitle, resetFAQ } = this.state;

    const runSpinner = showSpinner || !Boolean(campDetails);

    if (runSpinner) {
      return <ProgressBar text={"Camp is loading..."} />;
    }

    return (
      <React.Fragment>
        <Header
          text={this.state.header}
          leftIcon={require("@Assets/icons/arrow_back.png")}
          onPressLeft={() => navigation.navigate("AllCamps")}
        />
        <View style={styles.container}>
          <ButtonGroup
            onPress={this.updateIndex}
            selectedIndex={selectedIndex}
            buttons={BUTTONS}
            containerStyle={styles.tab}
            buttonStyle={styles.buttonTab}
            textStyle={styles.tabTextStyle}
            innerBorderStyle={styles.innerBorderStyle}
            selectedButtonStyle={styles.selectedButtonStyle}
            selectedTextStyle={styles.selectedTextStyle}
          />
          <Swiper
            ref={(swiper) => (this.swiper = swiper)}
            autoplay={false}
            showsButtons={false}
            loop={false}
            showsPagination={false}
            scrollEnabled={false}
            width={Layout.width}>
            <About
              ref={(scroll) => (this.aboutScroll = scroll)}
              details={campDetails.about}
              entitle={entitle}
            />
            <Rates
              ref={(scroll) => (this.ratesScroll = scroll)}
              details={campDetails.rates}
              goToGallery={() => this.updateIndex(2)}
            />
            <Gallery details={campDetails.gallery} />
            <Faq
              ref={(scroll) => (this.faqScroll = scroll)}
              details={campDetails.faq}
              reset={resetFAQ}
            />
          </Swiper>
          <View style={styles.bottomBar}>
            <Button
              onPress={this.goToCampMap}
              containerStyle={styles.bottomBarButtonStyle}
              buttonStyle={styles.bottomBarButtonStyle}
              titleStyle={styles.bottomBarLeftButtonStyle}
              icon={
                <Icon
                  type="material-community"
                  name="map-marker-radius"
                  size={20}
                  color={Colors.matterhorn}
                />
              }
              title="VIEW MAP"
            />
            <Button
              onPress={this.shareSocial}
              containerStyle={styles.bottomBarButtonStyle}
              buttonStyle={styles.bottomBarButtonStyle}
              icon={
                <Icon type="evilicon" name="share-google" size={24} color={Colors.matterhorn} />
              }
            />
          </View>
        </View>
      </React.Fragment>
    );
  }
}

export default connect(
  (state, { navigation }) => ({
    showSpinner: state.app.showSpinner,
    campDetails: state.camps[navigation.state.params.campName],
  }),
  (dispatch) => ({
    getCamp: (id) => dispatch(getCamp(id)),
    startSpinner: () => dispatch(startSpinner()),
  })
)(CampsNavigator);
